<html>
<base href="../">
<?php require '../objects/includes/header.php';?>
<?php require '../objects/includes/sidebar.php';?>
<body>
<div id="main">
<div id="pad">
<p>On this page I need you to tell me all about you're main content. I need to know what background color you want and what color your text should be.</p>
<br />
<form action="initial_setup/set_main.php" method="post">
	Background Color:<br /><?php $dropdown_name = 'main_background_color'; include '../objects/includes/color_dropdown.php';?>
	<br />
	Text Color:<br /><?php $dropdown_name = 'main_text_color'; include '../objects/includes/color_dropdown.php';?>
	<br />
	Main Content Height:<br /><input type="number" name="main_height" value="500" min="100"/>
	<br />
	<br />
	<input type="submit" value="Next" />
	
</form>

</div>
</div>
</body>
</html>
