<?php
	include "../site_info/variables.php";
	$main_width = $body_width-$sidebar_width;

//<!---------------------------------------- make css file ---------------------------------------->

	$file = fopen("../css/temp.css", "w");
	
	if($body_width != ""){
		fwrite($file, "body\n{\n	width:".$body_width."px;\n	margin-left:auto;\n	margin-right:auto;\n}\n");
	}
	if($sidebar == "yes"){
		fwrite($file, "#sidebar\n{\n");
		if($sidebar_width != ""){
			fwrite($file, "	width:$sidebar_width;\n");
		}
		if($sidebar_color != ""){
			fwrite($file, "	background-color:$sidebar_color;\n");
		}
		if($sidebar_height != ""){
			fwrite($file, "	height:$sidebar_height;\n");
		}
		fwrite($file, "	float:left;\n}\n");
		if($sidebar_text_color != ""){
			fwrite($file, "#sidebar_text_color\n{\n	color:$sidebar_text_color;\n}\n");
		}
		
	}
	
	if($header == "yes"){
		fwrite($file, "#header\n{\n	text-align:center;\n	font-size:250%;\n}\n");
	}
	fwrite($file, "#main\n{\n	background-color:$main_background_color;\n	color:$main_text_color;\n	float:left;\n	height:$main_height;\n	width:$main_width;\n}\n");
	if($background_color != ""){
		fwrite($file, "html\n{\n	background-color:$background_color;\n}\n");	
	}
	fclose($file);

//<!------------------------------------ make php files -------------------------------------------->

//	<!-- header file -->

	if($header == "yes"){
		$file = fopen("../objects/includes/temp_header.php", "w");
		fwrite($file, "<?php include 'site_info/variables.php'; ?>\n<html>\n<header>\n<link rel=".'"'."stylesheet".'"'." type=".'"'."text/css".'"'." href=".'"'."css/main.css".'"'.">\n");
		if($header_text != ""){
			fwrite($file, "<h1 id=".'"'."header".'"'.">".$header_text."</h1>\n");
		}
		fwrite($file, "</header>\n</html>\n");
        fclose($file);
        unlink("../objects/includes/header.php");
        rename("../objects/includes/temp_header.php", "../objects/includes/header.php");
	}

//	<!-- sidebar file -->

	if($sidebar == "yes"){
		$file = fopen("../objects/includes/temp_sidebar.php", "w");
		fwrite($file, "<?php include 'site_info/variables.php'; ?>\n<html>\n<header>\n<link rel=".'"'."stylesheet".'"'." type=".'"'."text/css".'"'." href=".'"'."css/temp.css".'"'.">\n</header>\n<body>\n<div id=".'"'."sidebar".'"'.">\n	<a href=".'"'."$domain".'"'." id=".'"'."sidebar_text_color".'"'.">Home</a>\n</div>\n</body>\n</html>");
		fclose($file);
        unlink("../objects/includes/sidebar.php");
        rename("../objects/includes/temp_sidebar.php", "../objects/includes/sidebar.php");
	}

//	<!-- index file -->

	$file = fopen("../temp_index.php", "w");
	fwrite($file, "<?php include 'site_info/variables.php'; ?>\n<?php include ".'"'."objects/includes/header.php".'"'."; ?>\n<?php include ".'"'."objects/includes/sidebar.php".'"'."; ?>\n<html>\n<header>\n<link rel=".'"'."stylesheet".'"'." type=".'"'."text/css".'"'." href=".'"'."css/temp.css".'"'.">\n</header>\n<body>\n<div id=".'"'."main".'"'."><p>This home page is your first page made with Simple. Please go to <a href=".'"'."administrator".'"'.">administrator.php</a> to continue editing this page and to add more pages.</p>\n</div>\n</body>\n</html>");
	fclose($file);
// <!-------------------------------------- delete stuff ----------------------------------------------->
    include "../objects/includes/delete_stuff.php";

    unlink("../index.php");
    rename("../temp_index.php", "../index.php");
    unlink("../css/main.css");
    rename("../css/temp.css", "../css/main.css");
    delTree("../initial_setup");

header( 'Location: ../' ) ;
	
	
?>
