<html>
<base href="../">
<?php require '../objects/includes/header.php';?>
<?php require '../objects/includes/sidebar.php';?>
<body>
<div id="main">
<div id="pad">
<p>Now I want to know what you want the background color of your website to be. In the end this will mostly only show around the edges of the screen.</p>
<br />
<form action="initial_setup/set_background.php" method="post">
	<?php $dropdown_name = 'color_dropdown'; include '../objects/includes/color_dropdown.php';?>
	<br />
	<br />
	<input type="submit" value="Next" />
	
</form>


</div>
</div>
</body>
</html>
