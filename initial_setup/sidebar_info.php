<html>
<base href="../">
<?php include '../site_info/variables.php';?>
<?php require '../objects/includes/header.php';?>
<?php require '../objects/includes/sidebar.php';?>
<body>
<div id="main">
<div id="pad">
<p>I want to know how wide you want your sidebar to be, remember that this is cutting into your main content. Then I'd like to know what color you want for the sidebar's background and for the sidebar's text.</p>
<br />
<form action="initial_setup/set_sidebar_info.php" method="post" enctype="multipart/form-data">
	<!-- Header Image: (only required if you are not using text)<br /><input type="file" name="file" />
	<br /> -->
	Sidebar Width:<br /><input type="number" name="sidebar_width" value="150" min="50" max="<?php echo( $body_width - 50);?>" />
	<br />
	Sidebar Height:<br /><input type="number" name="sidebar_height" value="500" min="100"/>
	<br />
	Sidebar Color:<br /><?php $dropdown_name = 'sidebar_color'; include '../objects/includes/color_dropdown.php';?>
	<br />
	Sidebar Text Color:<br /><?php $dropdown_name = 'sidebar_text_color'; include '../objects/includes/color_dropdown.php';?>
	<br />
	<br />
	<input type="submit" value="Next" />
	
</form>

</div>
</div>
</body>
</html>
